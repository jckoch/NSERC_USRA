% Created 2018-02-04 Sun 18:19
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\graphicspath{ {/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/} }
\usepackage[margin=0.7in]{geometry}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{setspace}
\doublespacing
\author{James C. Koch}
\date{\today}
\title{Evaluation of Flexural Coefficient of Mono-Symmetry in CSA S16 Design Standard}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 25.3.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle

\section{Introduction of Current Design Standard for Singly-Symmetric Beams}
\label{sec-1}

Current structural design standards \cite{CSAS16-14} in Canada for the flexural design of singly-symmetric beams are investigated with respect to potential errors in approximating the amount of mono-symmetry associated with such a beam cross-section.
It has been noted that for largely mono-symmetric cross-sections, the current design approximation accounting for this mono-symmetry seems to provide relatively large errors. 
This technical note aims to highlight the need for further research by comparing the current CSA S16 design code \cite{CSAS16-14} approximations for beam cross-section mono-symmetry with a calculus-based exact formulation.
The main reason for the current approximation is that the exact formulation is complicated and unwieldy to use efficiently in structural design and therefore it is of interest to investigate approximations for the beam mono-symmetry.
The primary objective of this technical note is to present structural engineers and designers with a analysis of the advantages and limitations of the approximated formula for \(\beta_x\) first developed by Kitipornchai and Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams} which has now been incorporated/modified into the CSA S16 Steel Standard \cite{CSAS16-14}.

\section{Theory of Lateral Torsional Buckling}
\label{sec-2}

The theory behind lateral torsional buckling (LTB) is governed by a set of differential equations relating the lateral deflection of a beam to a twisting angle associated with St. Venant pure torsion and a warping torsion arising from various boundary conditions.
The complete theory of LTB can be found for example in Structural Frames and Members by Galambos \cite{Galambos_1968_StructuralMembersAndFrames}.
The geometry of singly-symmetric W-sections can be seen in Figure \ref{fig-IsectionGeometry} along side the uniform bending load case considered in this technical note.

\begin{figure}[htb]
\centering
\includegraphics[width=0.5\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/SAMPLE-singlysymmetricIsection.jpg}
\caption{Labeled Singly-Symmetric I-Section \label{fig-IsectionGeometry}}
\end{figure}

It should be noted that the main difference in LTB theory of doubly- versus singly-symmetric beam cross-sections is that the shear center and geometric centroid do not coincide for singly-symmetric sections.
This is the primary reason for the introduction of torsional stresses within a cross-section of a singly-symmetric section which is reflected in the governing differential equations \cite{Galambos_1968_StructuralMembersAndFrames} which are presented in Equations \ref{eq-governingdifferentialequations}. 
It is understood that \(M_x = - M_{Bx} + \frac{z}{L} \left( M_{Tx} + M_{Bx} \right)\), where \(M_{Bx}\) and \(M_{Tx}\) are the end moments on the beam in the differential equations presented (Eqn. \ref{eq-governingdifferentialequations}).
Additionally, the analytical closed-form solution to the governing differential equations \ref{eq-governingdifferentialequations} exists only for beams subjected to uniform bending moments and shear center loading.
As such, this technical note will limit itself to the above described loading case.
For beams subjected to moment gradients and/or loading not at the shear centre, no closed-form analytical solutions exist and therefore numerical solutions must be employed; this is however out of the scope of this technical note.

\begin{align} 
\begin{split} \label{eq-governingdifferentialequations}
EI_y u^{iv} + M_x \alpha^{\prime\prime} + M_x^{\prime} \alpha^{\prime} &= 0 \\
EC_w \alpha^{iv} - (GJ + M_o \beta_x) \alpha^{\prime\prime} - M_x^{\prime} \beta_x \alpha^{\prime} + M_x u^{\prime\prime} &= 0 
\end{split}
\end{align}

From the governing differential equations (Eqn. \ref{eq-governingdifferentialequations}), \(\beta_x\) naturally arises from the unequal bending compressive and tensile stresses present in a cross-section of a singly-symmetric beam due to the unequal flange sizes.
The mono-symmetry of these sections can be generally expressed as in Equation \ref{eq-generalbetaX}.
This \(\beta_x\) mono-symmetry parameter can be expanded for common cross-sectional geometries such as singly-symmetric W-sections including WT-Sections and are given by Equation \ref{eq-singlysymmetricBetX} and Equation \ref{eq-TsectionBetX}, respectively.

\begin{align} \label{eq-generalbetaX}
\beta_x &= \frac{1}{I_x} \int_A y(x^2 + y^2)dA - 2y_o
\end{align}

\begin{align} \label{eq-singlysymmetricBetX}
\beta_x &= \frac{1}{I_x} \left[ (d^{\prime}-\bar{y}) \left( \frac{b_2^3 t_2}{12} + b_2 t_2 (d^{\prime}-\bar{y})^2 + (d^{\prime}-\bar{y})^3 \frac{w}{4} \right) - \bar{y} \left( \frac{b_1^3 t_1}{12} + b_1 t_1 \bar{y}^2 + \bar{y}^3 \frac{w}{4} \right) \right] - 2y_o
\end{align}

\begin{align} \label{eq-TsectionBetX}
\beta_x &= \frac{1}{I_x} \left[ \frac{w}{4} \left( (d - \bar{y_T})^4 - (\bar{y_T} - \frac{t}{2})^4 \right) - b t (\bar{y_T} - \frac{t}{2}) \left( \frac{b^2}{12} + (\bar{y_T} - \frac{t}{2})^2 \right) \right] - 2y_o
\end{align}

Additionally, the approximated \(\beta_x\) formulas derived from Kitipornchai \& Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams} and the current CSA S16 design standard \cite{CSAS16-14} are given in Equation \ref{eq-approxBetX_KandT} and Equation \ref{eq-approxBetX_S16}, respectively.

\begin{align} \label{eq-approxBetX_KandT}
\beta_x &= 0.9(d - t)\left(\frac{I_{yc}}{I_{yc} + I_{yt}} - 1\right)\left(1 - \left(\frac{I_y}{I_x}\right)^2\right)
\end{align}

\begin{align} \label{eq-approxBetX_S16}
\beta_x &= 0.9(d - t)\left(\frac{2I_{yc}}{I_y} - 1\right)\left(1 - \left(\frac{I_y}{I_x}\right)^2\right)
\end{align}

\section{Mono-Symmetry Property, \(\beta_x\), for Singly-Symmetric I-Sections}
\label{sec-3}

For the purpose of investigating the accuracy of the approximated \(\beta_x\) formula in Equation \ref{eq-approxBetX_KandT} and \ref{eq-approxBetX_S16}, the provided 281 doubly-symmetric W-sections available from the CISC Steel Design Handbook (aka "the Handbook") were used to generate 16312 singly-symmetric W-sections by simply reducing each flange separately by 10mm increments until each W-section merges into a T-section.
The properties of these generated singly-symmetric WT-sections (i.e. flange completely disappears) should be highlighted in that they do not necessarily reflect true WT-sections that are used in structural engineering practice as their webs are generally too deep.

Nonetheless, these generated W-sections provide a opportunity to discover the effect of the current \(\beta_x\) approximations.
The analysis shows that as the mono-symmetry grows so too does the percent error for the current \(\beta_x\) approximation.
This effect can be seen in Figure \ref{fig-singlysymmetricBetXerrors} where the x-axis represents the state of the mono-symmetry property, \(\rho = \frac{I_{yc}}{I_{yc} + I_{yt}}\), and the y-axis represents the percent error of \(\beta_x\).
The mono-symmetry parameter, \(\rho\), is defined such that for \(\rho = 0\) the section comprises of a inverted WT-section and for \(\rho = 1\) the section comprises of a WT-section.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/madeupSinglySymmetricIsections.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for all Made-Up Singly-Symmetric W-Sections \label{fig-singlysymmetricBetXerrors}}
\end{figure}

Figure \ref{fig-singlysymmetricBetXerrors} depicts significant errors when using the current form of the approximation (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-approxBetX_S16}) for \(\beta_x\) \cite{CSAS16-14,Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams} of highly mono-symmetric W-sections.
Looking closer at only the generated singly-symmetric W-sections which are geometrical representations of  WT-sections, Figure \ref{fig-allClassesMadeUpTSectionsBetXerrors} shows that the percent error is very similar. 

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/madeupTsections_allclasses.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of all Classes of Sections \label{fig-allClassesMadeUpTSectionsBetXerrors}}
\end{figure}

However, these WT-sections are not representative of those used in practice as these WT-sections are from the set of generated singly-symmetric W-sections and thus exist with deeper than normal webs.
This makes it more likely that these generated WT-sections are Class 4 whereby local buckling would govern.
Figure \ref{fig-sectionclasshist} shows the number of sections based on which class it is where the majority of sections are Class 1 but there do exist a relative high number of Class 2, 3, and 4 sections.

\begin{figure}[htb]
\centering
\includegraphics[width=0.5\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/countOfSectionClass.png}
\caption{Number of Sections as a function of Section Class \label{fig-sectionclasshist}}
\end{figure}

Therefore, Figure \ref{fig-Class1and2MadeUpTSectionBetXerrors} shows the percent error of only Class 1 and 2 generated WT-sections which reduces the number of sections considerably (i.e. many of the WT-sections generated end up as either Class 3 or 4) but the errors in \(\beta_x\) still persist.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/madeupTsections_class1and2.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of only Class 1 and 2 Sections \label{fig-Class1and2MadeUpTSectionBetXerrors}}
\end{figure}

Since these WT-sections are not standard sizes, the next section of this technical note will investigate the effect of using the approximations defined above for \(\beta_x\) on standard WT-sections as provided from the Handbook.
\section{Mono-Symmetry Property, \(\beta_x\), for standard WT-Sections}
\label{sec-4}

For standard WT-sections from the Handbook, the mono-symmetry property is given in the tables provided.
The mono-symmetry property in the Handbook is calculated to include the effect of fillets in either hot-rolled or cold-formed W- and WT-sections.
As a simplification, this technical note neglects the effect of fillets on the mono-symmetry property (Eqn. \ref{eq-TsectionBetX}) which is warranted as a comparison is conducted on the difference which can be seen in Figure \ref{fig-HandbookVSGalambosTSectionBetXComparison}.
The effect of fillets on \(\beta_x\) is less than 1\% and therefore will be neglected within the scope of this technical note.

\begin{figure}[htb]
\centering
\includegraphics[width=0.5\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/HandbookVSGalamboTSectionBetXComparison.png}
\caption{Percent Difference of Including the Effect of Fillets in Hot-rolled or Cold-formed W-/WT-Sections on the Mono-symmetry property, \(\beta_x\) \label{fig-HandbookVSGalambosTSectionBetXComparison}}
\end{figure}

For standard WT-sections, the approximations for \(\beta_x\) perform even worse with percent errors up to 14000\% as shown in Figure \ref{fig-TsectionBetX_plusClass}.
Looking more closely at the distribution of section classes with respect to the \(\beta_x\) percent error, the section class does not seem to influence the accuracy of the \(\beta_x\) approximations as shown in Figure \ref{fig-TsectionBetX_plusClass}

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/TsectionBetX_plusClass.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Standard WT-Sections for all Classes of Sections (left) and only Class 3 and 4 Sections (right) \label{fig-TsectionBetX_plusClass}}
\end{figure}

\section{Discussion}
\label{sec-5}

From the analysis of the mono-symmetry of W-shaped beam cross-sections, it has been found that significant errors in the mono-symmetry parameter, \(\beta_x\), exist.
The results will be explored on the basis of singly-symmetric W-sections and WT-sections separately.
This technical note will explore potential mechanical/mathematical interpretation that may explain this phenomenon.

By plotting the \(\beta_x\) characteristic of a W1100x499 section as each flange decrease in width, the shape of the \(\beta_x\) versus \(\rho\) curve becomes evident as seen in Figure \ref{fig-W1100x499}.
At the extreme mono-symmetric sections, there exists a slight decrease in the value of the mono-symmetry parameter, \(\beta_x\). 
This slight hook is present on all 281 doubly-symmetric sections when the flange widths are reduced.
The hook seems to occur for both T-sections and inverted T-sections.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/W1100x499_BetxVSrho.png}
\caption{\(\beta_x\) as a function of \(\rho\) for a W1100x499 Section \label{fig-W1100x499}}
\end{figure}

Furthermore, on close inspection of the percent errors from the approximation by Kitipornchai and Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams} and CSA S16-14 \cite{CSAS16-14} it can be shown that the S16-14 approximation does not provide symmetrical results as compared to approximation by Kitipornchai and Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams}. 
This means that the CSA S16-14 \cite{CSAS16-14} approximation performs differently for inverted and non-inverted T-sections.

Mathematically, the percent error present for highly mono-symmetric sections by using the approximated formula exists since \(\beta_x\) is not linear from \(0 \leq \rho \leq 1.0\).
This would raise the possibility of prescribing the approximation with limits based on \(\rho\) which is a potential solution; however, sections which do not fall within the said prescribed limits would need to be treated separately.
Additionally, the limits of \(0.1 \leq \rho \leq 0.9\) has been analyzed and Figure \ref{fig-limitedrhoSinglySymmetricSections} shows the results of limiting the sections to these limits.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/madeupSinglySymmetricIsections-limitedrho.png}
\caption{Percent Error of \(\beta_x\) Approximations limited by \(0.1 \leq \rho \leq 0.9\) \label{fig-limitedrhoSinglySymmetricSections}}
\end{figure}

Additionally, from earlier analyses of W- and WT-sections in Figures \ref{fig-allClassesMadeUpTSectionsBetXerrors} and \ref{fig-TsectionBetX_plusClass} there exists a significant trend whereby large \(\frac{I_y}{I_x}\) ratios tend towards higher errors in \(\beta_x\).
Kitipornchai and Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams} also limit the use of their developed approximation to sections with \(\frac{I_y}{I_x} \leq 0.5\). 
By limiting the ratio of \(\frac{I_y}{I_x}\) in conjunction with sections which do not fall within the limits of \(0.1 \leq \rho \leq 0.9\), it has been shown in Figure that the percent errors for these types of sections can be reduced below 25\%.

The above mentioned solutions do not dismiss concerns about the suitability of the approximated forms of the \(\beta_x\) equations (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-approxBetX_S16}) but provide a temporary measure to limit the amount of error that is being introduced into the calculation of a elastic critical moment of mono-symmetrical beams.
Further research is needed to provide a better approximated formula for \(\beta_x\) and quantifying the effect in more detail on the elastic critical moment capacity of singly-symmetric beams using the approximated \(\beta_x\) equations (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-approxBetX_S16}).

\subsection{Specific Discussion of WT-Sections}
\label{sec-5-1}

The approximations for \(\beta_x\) in Equations \ref{eq-approxBetX_KandT} and \ref{eq-approxBetX_S16} provide higher percent errors for WT-sections than for the "made-up" singly-symmetric sections previously discussed. 
The subplot on the left of Figure \ref{fig-WTsectionPercentError} shows the percent error as a function of the \(\frac{I_y}{I_x}\) ratio as all of these sections have a value of \(\rho = 1.0\).
The remaining subplot on the right of Figure \ref{fig-WTsectionPercentError} shows the percent error again as a function of \(\frac{I_y}{I_x}\) ratio but limited at \(\frac{I_y}{I_x} < 0.5\).

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/TsectionBetXmotivation.png}
\caption{Percent Error of \(\beta_x\) as a function of \(\frac{I_y}{I_x}\) Ratio for WT-Sections \label{fig-WTsectionPercentError}}
\end{figure}

The mathematical explanation for this result is that the \(\frac{I_y}{I_x}\) ratio is proportional to \(\beta_x\) by a power of two which dramatically increases the \(\beta_x\) for high values of the \(\frac{I_y}{I_x}\) ratio.

\section{Recommendation/Conclusion}
\label{sec-6}

In conclusion, further in detail studies are required to determine a more appropriate approximation for \(\beta_x\).
One potential avenue of research is to fit a splines to the true \(\beta_x\) curves with respect to the mono-symmetry, \(\rho\).
However in the interim, it is recommended that the condition of \(\frac{I_y}{I_x} < 0.5\) be included in any use of the current approximations, particularly for WT-sections, as this was the limitation placed on the approximation by the original research by Kitipornchai and Trahair \cite{Kitipornchai_1979_BucklingPropertiesMonoSymmetricIBeams}.
This technical note recommends that to limit the use of the current \(\beta_x\) approximation to \(0.1 < \rho < 0.9\) as significant errors are evident outside of these limits on singly-symmetric sections.

\bibliographystyle{unsrt}
\bibliography{../../../../../../home/jkoch/Documents/references/REFERENCES}
% Emacs 25.3.1 (Org mode 8.2.10)
\end{document}