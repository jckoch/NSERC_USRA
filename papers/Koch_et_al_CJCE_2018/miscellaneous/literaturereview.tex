% Created 2018-02-26 Mon 13:28
\documentclass[letter, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\graphicspath{ {/winc/jk/UofA/Courses/project_DRA/AnalysisBetX/figs/} }
\usepackage[margin=0.7in]{geometry}
\usepackage{graphicx}
\usepackage{setspace}
\doublespacing
\author{James Koch}
\date{\textit{<2018-02-17 Sat>}}
\title{literaturereview}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={}}
\begin{document}

\maketitle

\section{Introduction}
\label{sec-1}
Lateral torsional buckling is often the limiting case of deep I-shaped girders in typical bridge construction.
This phenomenon must therefore be understood by the structural designer in order to ensure a safe and efficient design.
The properties of lateral torsional buckling of doubly-symmetric I-sections is a well-researched area and detailed analysis procedures exist in modern structural steel design standards \cite{s16-14_design_2014,aisc_360-16_aisc_2016}.
However, lateral torsional buckling of singly-symmetric I-sections have been less researched and simplified analysis procedures if at all have been included in modern structural steel design standards \cite{s16-14_design_2014,aisc_360-16_aisc_2016}.
A early theoretical overview of the challenges of singly-symmetric I-sections is introduced by Anderson and Trahair \cite{anderson_stability_1972} who go on to provide early experimental results.

The main phenomenon behind the difference between doubly-symmetric and singly-symmetric I-sections is the so-called "Wagner effect" \cite{Wagner_1936_VerdrehungUndKnickung}.
The Wagner effect is produced by the twisting of a member which causes axial compressive stresses to exert a disturbing torque .
This torque can be considered to increase/decrease the torsional stiffness of the member depending on the sign of the axial force.
In general, the Wagner effect was first determined for torsional buckling in columns; however, the concept applies to lateral torsional buckling in beams.
For doubly-symmetric beam cross-sections, the equal nature of axial compressive and tensile stresses balance and thus the Wagner effect is not observed \cite{anderson_stability_1972}.
In singly-symmetric beam cross-sections, the axial compressive and tensile stresses do not balance and the Wagner effect is observed whereby these singly-symmetric beams experience either an increase or decrease in torsional stiffness.

Specifically the modern Canadian structural steel design standard (CSA S16-14) \cite{s16-14_design_2014} take the Wagner effect into account by introducing a beam asymmetry parameter, \(\beta_x\), calculated by a approximated formula.
The formulation of a analytical solution to the lateral torsional buckling differential equations for a simply-supported singly-symmetric beam does not exist and therefore it is of interest to structural designers to develop approximate formulae for design purposes.
It has been noted that for largely singly-symmetric cross-sections, the current design approximation accounting for the monosymmetry seems to provide relatively large errors in the monosymmetry parameter, \(\beta_x\).
This technical note aims to highlight the need for further research by comparing the current CSA S16-14 design standard \cite{s16-14_design_2014} monosymmetry parameter approximation with it's exact calculus-based solution.
This paper has the following layout: in \hyperref[sec-2]{Section 2} the existing literature is reviewed, in \hyperref[sec-3-1]{Section 3.1} and \hyperref[sec-3-2]{Section 3.2} results of the comparison study for singly-symmetric W- and WT- sections are reported, in \hyperref[sec-4]{Section 5} a discussion of the results is presented, and in \hyperref[sec-5]{Section 6} our conclusions are presented.

\section{Literature Review}
\label{sec-2}
The basis of the monosymmetry parameter, \(\beta_x\), in the current CSA S16-14 design standard is derived from the work by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979}.
The approximation derived by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} shown in Equation  is developed from a purely theoretical basis and a comparison with the existing literature is used as justification.

\begin{align} \label{eq-approxBetX_KandT}
\beta_x &= 0.9(d - t)\left(2 \frac{I_{yc}}{I_{yc} + I_{yt}} - 1\right)\left(1 - \left(\frac{I_y}{I_x}\right)^2\right)
\end{align}

To provide context of the approximation derived by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979}, it is necessary to delve into the theoretical background of the phenomenon of lateral torsional buckling in beams.
A fairly complete and comprehensive review of lateral torsional buckling can be found in standard structural engineering texts such as Structural Frames and Members by Galambos \cite{galambos_structural_1968}.

Lateral torsional buckling is governed by a set of differential equations relating the lateral deflection of a beam to a twisting angle associated with St. Venant pure torsion and a warping torsion arising from various boundary conditions.
The geometry of singly-symmetric I-section is shown in Figure \ref{fig-IsectionGeometry} as well as the uniform bending load case for a simply-supported beam.

\begin{figure}[htb]
\centering
\includegraphics[width=0.7\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/generic_Wsection_dimensions.eps}
\caption{Labeled Singly-Symmetric I-Section \label{fig-IsectionGeometry}}
\end{figure}

As introduced above, the main difference between doubly-symmetric and singly-symmetric beam cross-sections is that the shear centre and geometric centroid do not coincide for singly-symmetric beams which gives rise to the unequal axial compressive and tensile stresses known as the Wagner effect.
The Wagner effect is the phenomenon behind the introduction of torsional stresses within a beam with a singly-symmetric cross-section.
Additionally, \(\beta_x\) logically arises for singly-symmetric cross-sections due to the existence of unequal flange widths which again indicates the unbalanced axial compressive and tensile stresses explained by the Wagner effect.
These torsional stresses are reflected in the set of governing differential equations presented in Equation \ref{eq-governingdifferentialequations}.
It is understood that \(M_x = - M_{Bx} + \frac{z}{L} \left( M_{Tx} + M_{Bx} \right)\), where \(M_{Bx}\) and \(M_{Tx}\) are the end moments on the simply-supported beam in the differential equations presented in Equation \ref{eq-governingdifferentialequations}.

\begin{align} 
\begin{split} \label{eq-governingdifferentialequations}
EI_y u^{iv} + M_x \alpha^{\prime\prime} + M_x^{\prime} \alpha^{\prime} &= 0 \\
EC_w \alpha^{iv} - (GJ + M_o \beta_x) \alpha^{\prime\prime} - M_x^{\prime} \beta_x \alpha^{\prime} + M_x u^{\prime\prime} &= 0 
\end{split}
\end{align}

Additionally, the analytical closed-form solution to the governing differential equations \ref{eq-governingdifferentialequations} exists only for beams subjected to uniform bending moments while the loading is applied above or below the shear centre.
As such, this technical note will limit itself to the above described loading case which can also be seen in Figure \ref{fig-IsectionGeometry}.
For beams subjected to moment gradients and/or loading not applied at the shear centre, no closed-form analytical solutions exist and therefore numerical solutions must be employed; this is however out of the scope of this technical note.

In order to derive the closed-form solution to Equation \ref{eq-governingdifferentialequations} for the loading case of uniform bending, it is necessary to determine the form of the monosymmetry parameter, \(\beta_x\), which is shown in it's integral form in Equation \ref{eq-betaXgeneralformulation}.
For singly-symmetric I-sections and T-sections, the \(\beta_x\) parameter can be expanded based on the standard geometry of a I- or T-section as shown in Equation \ref{eq-betaXexpandedformulation} and \ref{eq-betaXexpandedTsectionformulation}, respectively.

\begin{align} 
\beta_x &= \frac{1}{I_x} \int_A y(x^2 + y^2)dA - 2y_o \label{eq-betaXgeneralformulation} \\
\beta_x &= \frac{1}{I_x} \left[ (h-\bar{y}) \left( \frac{b_2^3 t_2}{12} + b_2 t_2 (h-\bar{y})^2 + (h-\bar{y})^3 \frac{w}{4} \right) - \bar{y} \left( \frac{b_1^3 t_1}{12} + b_1 t_1 \bar{y}^2 + \bar{y}^3 \frac{w}{4} \right) \right] - 2y_o  \label{eq-betaXexpandedformulation} \\
\beta_x &= \frac{1}{I_x} \left[ \frac{w}{4} \left( (d - \bar{y_T})^4 - (\bar{y_T} - \frac{t}{2})^4 \right) - b t (\bar{y_T} - \frac{t}{2}) \left( \frac{b^2}{12} + (\bar{y_T} - \frac{t}{2})^2 \right) \right] - 2y_o \label{eq-betaXexpandedTsectionformulation}
\end{align}

Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} developed their approximated equation \ref{eq-betaXapproximation_KandT} by beginning with the special case of a singly-symmetric web-less I-beam.
In making the assumption that the web of a I-beam provides very little influence on the torsional stiffness of the overall cross-section, Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} were able to significantly simplify Equation \ref{eq-betaXexpandedformulation} to Equation \ref{eq-betaXweblessIbeamKandT}.

\begin{align} \label{eq-betaXweblessIbeamKandT}
\beta_X &= \frac{1}{I_x} \left[ (h-\bar{y}) \left( \frac{b_2^3 t_2}{12} + b_2 t_2 (h-\bar{y})^2 \right) - \bar{y} \left( \frac{b_1^3 t_1}{12} + b_1 t_1 \bar{y}^2 \right) \right] - 2y_o
\end{align}

Having derived the ratio of the flange to total cross-sectional area as \(\mu = \frac{A_{fc}}{A_{fc}+A_{ft}}\), ratio of the area moment of inertia of both flanges to the total moment of inertia as \(\rho = \frac{I_{yc}}{I_{yc}+I_{yt}}\), and the relationship with the location of the shear centre as \(y_o = (\mu - \rho) h\), Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} then combine Equations \ref{eq-betaXweblessIbeamKandT}, \ref{eq-IxvsA}, \ref{eq-yovsmuandrho} to produce their approximated formula shown in Equation \ref{eq-betaXweblessIbeamapproxKandT}.

\begin{align}
I_x &= A_{fc} (1 - \mu)^2 h^{2} + A_{fc} \mu^2 h^2  = \mu (1 - \mu) A h^2 \label{eq-IxvsA} \\
y_o &= (\mu - \rho) h \label{eq-yovsmuandrho} \\
\frac{\beta_X}{h} &= (2\rho - 1) + \frac{I_y}{I_X} (\mu - \rho) \approx (2\rho - 1) \text{, when } I_y \lll I_x  \label{eq-betaXweblessIbeamapproxKandT}
\end{align}

Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} then show that it is not sufficient to neglect the contribution of the web in a real I-beam especially to calculate the major axis moment of inertia, \(I_x\).
In order to obtain their final approximated formula shown in Equation \ref{eq-approxBetX_KandT}, Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} included the ratio of the major to minor moments of inertia for a wide range of plate girder dimensions.

In particular, the combination of cross-sectional dimensions Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} included consisted of flange width to thickness ratios of \(4 \leq \frac{B}{T} \leq 64\) and flange thickness to web thickness ratios of \(10 \leq \frac{T}{W} \leq 290\). 
Of particular note, Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} specifically neglect beam cross-sections where \(\frac{I_y}{I_x} > 0.5\) without a further explanation.
Given these above mentioned conditions, Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} express their approximated formula for \(\beta_x\) as per Equation \ref{eq-approxBetX_KandT} and report a zero mean error with a standard deviation of 0.037 for over 3000 beam cross-sections considered.

The current CSA S16-14 standard \cite{s16-14_design_2014} contains a adapted formula for \(\beta_x\) seen in Equation \ref{eq-CSAS16}. 
The CSA S16-14 standard \cite{s16-14_design_2014} refers to the book by Ziemian \cite{ziemian_guide_2010} which illustrates the use of the approximation by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} in Equation \ref{eq-approxBetX_KandT}.

\begin{align}
\beta_x &= 0.9 (d - t) (\frac{2 I_{yc}}{I_y} - 1) \left( 1 - left( \frac{I_y}{I_x} \right) \right) \label{eq-CSAS16}
\end{align}

\section{Comparison Study of the \(\beta_x\) Monosymmetry Parameter}
\label{sec-3}
For the purpose of investigating the accuracy of the approximated \(\beta_x\) formula in Equation \ref{eq-approxBetX_KandT} and \ref{eq-CSAS16}, the provided 281 doubly-symmetric W-sections available from the CISC Steel Design Handbook (aka "the Handbook") were used to generate 16312 singly-symmetric W-sections by separately reducing each flange by 10mm increments until each W-section merges into a T-section.
These generated sections provide the starting point for the comparison study of \(\beta_x\) 
A reminder in terms of the properties of these generated singly-symmetric WT-sections (i.e. flange completely disappears) do not necessarily reflect standard WT-sections used in structural engineering practice as their webs are generally too deep; however, they produce good estimates for all other monosymmetric sections.
Nonetheless, these generated W-sections provide a opportunity to discover the effect of the current \(\beta_x\) approximations.

In the second part of this comparison study, the approximated formula in Equation \ref{eq-approxBetX_KandT} and \ref{eq-CSAS16} is compared to the exact Equation \ref{eq-betaXexpandedTsectionformulation} for T-sections.

\subsection{Singly-Symmetric W-Sections}
\label{sec-3-1}
The analysis for unequal flange W-sections shows that as the mono-symmetry grows so too does the percent error for the current \(\beta_x\) approximation.
This effect can be seen in Figure \ref{fig-singlysymmetricBetXerrors} where the x-axis represents the state of the mono-symmetry property, \(\rho = \frac{I_{yc}}{I_{yc} + I_{yt}}\), and the y-axis represents the percent error of \(\beta_x\).
The mono-symmetry parameter, \(\rho\), is defined such that for \(\rho = 0\) the section comprises of a inverted WT-section and for \(\rho = 1\) the section comprises of a WT-section.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/madeupSinglySymmetricIsections.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for all Made-Up Singly-Symmetric W-Sections \label{fig-singlysymmetricBetXerrors}}
\end{figure}

Figure \ref{fig-singlysymmetricBetXerrors} depicts significant errors when using the current form of the approximation (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-CSAS16}) for \(\beta_x\) \cite{s16-14_design_2014,kitipornchai_buckling_1979} of highly mono-symmetric W-sections.
Looking closer at only the generated singly-symmetric W-sections which are geometrical representations of  WT-sections, Figure \ref{fig-allClassesMadeUpTSectionsBetXerrors} shows that the percent error is very similar. 

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/madeupTsections_allclasses.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of all Classes of Sections \label{fig-allClassesMadeUpTSectionsBetXerrors}}
\end{figure}

However, these WT-sections are not representative of those used in practice as these WT-sections are from the set of generated singly-symmetric W-sections and thus exist with deeper than normal webs.
This makes it more likely that these generated WT-sections are Class 4 whereby local buckling would govern.
Figure \ref{fig-sectionclasshist} shows the number of sections based on which class it is where the majority of sections are Class 1 but there do exist a relative high number of Class 2, 3, and 4 sections.

\begin{figure}[htb]
\centering
\includegraphics[width=0.5\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/countOfSectionClass.png}
\caption{Number of Sections as a function of Section Class \label{fig-sectionclasshist}}
\end{figure}

Therefore, Figure \ref{fig-Class1and2MadeUpTSectionBetXerrors} shows the percent error of only Class 1 and 2 generated WT-sections which reduces the number of sections considerably (i.e. many of the WT-sections generated end up as either Class 3 or 4) but the errors in \(\beta_x\) still persist.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/madeupTsections_class1and2.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of only Class 1 and 2 Sections \label{fig-Class1and2MadeUpTSectionBetXerrors}}
\end{figure}

Since these WT-sections are not standard sizes, the next section of this technical note will investigate the effect of using the approximations defined above for \(\beta_x\) on standard WT-sections as provided from the Handbook.

\subsection{Standard WT-Sections}
\label{sec-3-2}
For standard WT-sections from the Handbook, the mono-symmetry property is given in the tables provided.
The mono-symmetry property in the Handbook is calculated to include the effect of fillets in either hot-rolled or cold-formed W- and WT-sections.
As a simplification, this technical note neglects the effect of fillets on the mono-symmetry property (Eqn. \ref{eq-TsectionBetX}) which is warranted as a comparison is conducted on the difference which can be seen in Figure \ref{fig-HandbookVSGalambosTSectionBetXComparison}.
The effect of fillets on \(\beta_x\) is less than 1\% and therefore will be neglected within the scope of this technical note.

\begin{figure}[htb]
\centering
\includegraphics[width=0.5\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/HandbookVSGalamboTSectionBetXComparison.png}
\caption{Percent Difference of Including the Effect of Fillets in Hot-rolled or Cold-formed W-/WT-Sections on the Mono-symmetry property, \(\beta_x\) \label{fig-HandbookVSGalambosTSectionBetXComparison}}
\end{figure}

For standard WT-sections, the approximations for \(\beta_x\) perform even worse with percent errors up to 14000\% as shown in Figure \ref{fig-TsectionBetX_plusClass}.
Looking more closely at the distribution of section classes with respect to the \(\beta_x\) percent error, the section class does not seem to influence the accuracy of the \(\beta_x\) approximations as shown in Figure \ref{fig-TsectionBetX_plusClass}

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/TsectionBetX_plusClass.png}
\caption{Percent Error of \(\beta_x\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Standard WT-Sections for all Classes of Sections (left) and only Class 3 and 4 Sections (right) \label{fig-TsectionBetX_plusClass}}
\end{figure}

\section{Discussion}
\label{sec-4}
From the analysis of the mono-symmetry of W-shaped beam cross-sections, it has been found that significant errors in the mono-symmetry parameter, \(\beta_x\), exist.
The results will be explored on the basis of singly-symmetric W-sections and WT-sections separately.
This technical note will explore potential mechanical/mathematical interpretation that may explain this phenomenon.

By plotting the \(\beta_x\) characteristic of a W1100x499 section as each flange decrease in width, the shape of the \(\beta_x\) versus \(\rho\) curve becomes evident as seen in Figure \ref{fig-W1100x499}.
At the extreme mono-symmetric sections, there exists a slight decrease in the value of the mono-symmetry parameter, \(\beta_x\). 
This slight hook is present on all 281 doubly-symmetric sections when the flange widths are reduced.
The hook seems to occur for both T-sections and inverted T-sections.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/W1100x499_BetxVSrho.png}
\caption{\(\beta_x\) as a function of \(\rho\) for a W1100x499 Section \label{fig-W1100x499}}
\end{figure}

Furthermore, on close inspection of the percent errors from the approximation by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} and CSA S16-14 \cite{s16-14_design_2014} it can be shown that the S16-14 approximation does not provide symmetrical results as compared to approximation by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979}. 
This means that the CSA S16-14 \cite{s16-14_design_2014} approximation performs differently for inverted and non-inverted T-sections.

Mathematically, the percent error present for highly mono-symmetric sections by using the approximated formula exists since \(\beta_x\) is not linear from \(0 \leq \rho \leq 1.0\).
This would raise the possibility of prescribing the approximation with limits based on \(\rho\) which is a potential solution; however, sections which do not fall within the said prescribed limits would need to be treated separately.
Additionally, the limits of \(0.1 \leq \rho \leq 0.9\) has been analyzed and Figure \ref{fig-limitedrhoSinglySymmetricSections} shows the results of limiting the sections to these limits.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{/winc/jk/UofA/Courses/project_DRA/analysis/AnalysisBetX/figs/madeupSinglySymmetricIsections-limitedrho.png}
\caption{Percent Error of \(\beta_x\) Approximations limited by \(0.1 \leq \rho \leq 0.9\) \label{fig-limitedrhoSinglySymmetricSections}}
\end{figure}

Additionally, from earlier analyses of W- and WT-sections in Figures \ref{fig-allClassesMadeUpTSectionsBetXerrors} and \ref{fig-TsectionBetX_plusClass} there exists a significant trend whereby large \(\frac{I_y}{I_x}\) ratios tend towards higher errors in \(\beta_x\).
Kitipornchai and Trahair \cite{kitipornchai_buckling_1979} also limit the use of their developed approximation to sections with \(\frac{I_y}{I_x} \leq 0.5\). 
By limiting the ratio of \(\frac{I_y}{I_x}\) in conjunction with sections which do not fall within the limits of \(0.1 \leq \rho \leq 0.9\), it has been shown in Figure that the percent errors for these types of sections can be reduced below 25\%.

The above mentioned solutions do not dismiss concerns about the suitability of the approximated forms of the \(\beta_x\) equations (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-CSAS16}) but provide a temporary measure to limit the amount of error that is being introduced into the calculation of a elastic critical moment of mono-symmetrical beams.
Further research is needed to provide a better approximated formula for \(\beta_x\) and quantifying the effect in more detail on the elastic critical moment capacity of singly-symmetric beams using the approximated \(\beta_x\) equations (Eqn. \ref{eq-approxBetX_KandT} and \ref{eq-CSAS16}).

\section{Conclusion}
\label{sec-5}
In conclusion, further in detail studies are required to determine a more appropriate approximation for \(\beta_x\).
One potential avenue of research is to fit a splines to the true \(\beta_x\) curves with respect to the mono-symmetry, \(\rho\).
However in the interim, it is recommended that the condition of \(\frac{I_y}{I_x} < 0.5\) be included in any use of the current approximations, particularly for WT-sections, as this was the limitation placed on the approximation by the original research by Kitipornchai and Trahair \cite{kitipornchai_buckling_1979}.
This technical note recommends that to limit the use of the current \(\beta_x\) approximation to \(0.1 < \rho < 0.9\) as significant errors are evident outside of these limits on singly-symmetric sections.

\bibliographystyle{unsrt}
\bibliography{../../../../../../../../home/jkoch/Documents/references/structuralengineering}
\end{document}