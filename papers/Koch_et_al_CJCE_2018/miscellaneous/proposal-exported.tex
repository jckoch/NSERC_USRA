\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
    \usepackage{xltxtra,xunicode}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
  \newcommand{\euro}{€}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\usepackage[usenames,dvipsnames]{color}
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={James C. Koch},
            pdftitle={Evaluation of Flexural Coefficient of Mono-Symmetry in CSA S16 Design Standard},
            colorlinks=true,
            citecolor=blue,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

\title{Evaluation of Flexural Coefficient of Mono-Symmetry in CSA S16 Design Standard}
\author{James C. Koch}
\date{}

% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

\begin{document}
\maketitle

\section{Introduction of Current Design Standard for Singly-Symmetric Beams}\label{introduction-of-current-design-standard-for-singly-symmetric-beams}

Current structural steel design standards S16-14 {[}1{]} in Canada for the flexural design of singly-symmetric beams are investigated with respect to potential errors in approximating the amount of mono-symmetry associated with such a beam cross-section. It has been noted that for largely mono-symmetric cross-sections, the current design approximation accounting for this mono-symmetry seems to provide relatively large errors. This technical note aims to highlight the need for further research by comparing the current Canadian Steel design code, CSA S16-14 {[}1{]} approximations for beam cross-section mono-symmetry with a calculus-based exact formulation. The main reason for the current approximation is that the exact formulation is complicated and unwieldy to use efficiently in structural design. Therefore it is of interest to investigate approximations for the beam mono-symmetry parameter. The primary objective of this technical note is to present structural engineers and designers with an analysis of the advantages and limitations of the approximated formula for \(\beta_{x}\) first developed by Kitipornchai and Trahair {[}2{]} which has now been incorporated/modified into the CSA S16 Steel Standard S16-14 {[}1{]}.

\section{Theory of Lateral Torsional Buckling}\label{theory-of-lateral-torsional-buckling}

The theory behind lateral torsional buckling (LTB) is governed by a set of differential equations relating the lateral deflection of a beam to a twisting angle associated with St. Venant pure torsion and a warping torsion arising from various boundary conditions. The complete theory of LTB can be found for example in Structural Frames and Members by Galambos {[}3{]}. The geometry of singly-symmetric W-sections can be seen in Figure 1 alongside the uniform bending load case considered in this technical note.

\includegraphics{media/image1.jpg}

\hyperdef{}{ux5fRef505539705}{\label{ux5fRef505539705}}{}Figure Labeled Singly-Symmetric I-Section

It should be noted that the main difference in LTB theory of doubly- versus singly-symmetric beam cross-sections is that the shear center and geometric centroid do not coincide for singly-symmetric sections. This is the primary reason for the introduction of torsional stresses within a cross-section of a singly-symmetric section which is reflected in the governing differential equations Galambos {[}3{]} which are presented in Equation 1. It is understood that \(M_{x} = - M_{\text{Bx}} + \frac{z}{L}\left( M_{\text{Tx}} + M_{\text{Bx}} \right)\), where \(M_{\text{Bx}}\) and \(M_{\text{Tx}}\) are the end moments on the beam in the differential equations presented (Eqn. 1). Additionally, the analytical closed-form solution to the governing differential equations exists only for beams subjected to uniform bending moments and shear center loading. As such, this technical note will limit itself to the above described loading case. For beams subjected to moment gradients and/or loading above or below the shear centre, no closed-form analytical solutions exist and therefore numerical solutions must be employed; this is however out of the scope of this technical note.

\(\begin{matrix}
\begin{matrix}
EI_{y}u^{\text{iv}} + M_{x}\alpha^{''} + M_{x}^{'}\alpha^{'} & \\
EC_{w}\alpha^{\text{iv}} - \left( GJ + M_{o}\beta_{x} \right)\alpha^{''} - M_{x}^{'}\beta_{x}\alpha^{'} + M_{x}u^{''} & \\
\end{matrix} \\
\end{matrix}\) (1)

From the governing differential equations, \(\beta_{x}\) naturally arises from the unequal bending compressive and tensile stresses present in a cross-section of a singly-symmetric beam due to the unequal flange sizes. The mono-symmetry of these sections can be generally expressed as in Equation 2. This mono-symmetry parameter, , can be expanded for common cross-sectional geometries such as singly-symmetric W-sections including WT-sections and are given by Equation 3 and Equation 4, respectively.

\(\begin{matrix}
\beta_{x} & = \frac{1}{I_{x}}\int_{A}^{}y(x^{2} + y^{2})dA - 2y_{o} \\
\end{matrix}\) (2)

\(\begin{matrix}
\beta_{x} & = \frac{1}{I_{x}}\left\lbrack (d^{'} - \overline{y})\left( \frac{b_{2}^{3}t_{2}}{12} + b_{2}t_{2}(d^{'} - \overline{y})^{2} + (d^{'} - \overline{y})^{3}\frac{w}{4} \right) - \overline{y}\left( \frac{b_{1}^{3}t_{1}}{12} + b_{1}t_{1}{\overline{y}}^{2} + {\overline{y}}^{3}\frac{w}{4} \right) \right\rbrack - 2y_{o} \\
\end{matrix}\)(3)

\(\begin{matrix}
\beta_{x} & = \frac{1}{I_{x}}\left\lbrack \frac{w}{4}\left( (d - \overline{y_{T}})^{4} - (\overline{y_{T}} - \frac{t}{2})^{4} \right) - bt(\overline{y_{T}} - \frac{t}{2})\left( \frac{b^{2}}{12} + (\overline{y_{T}} - \frac{t}{2})^{2} \right) \right\rbrack - 2y_{o} \\
\end{matrix}\) (4)

Additionally, the approximated \(\beta_{x}\) formulas derived from Kitipornchai \& Trahair Kitipornchai and Trahair {[}2{]} and the current CSA S16 design standard {[}1{]} are given in Equation 5 and Equation 6, respectively.

\(\begin{matrix}
\beta_{x} & = 0.9(d - t)\left( \frac{I_{\text{yc}}}{I_{\text{yc}} + I_{\text{yt}}} - 1 \right)\left( 1 - \left( \frac{I_{y}}{I_{x}} \right)^{2} \right) \\
\end{matrix}\) (5)

\(\begin{matrix}
\beta_{x} & = 0.9(d - t)\left( \frac{2I_{\text{yc}}}{I_{y}} - 1 \right)\left( 1 - \left( \frac{I_{y}}{I_{x}} \right)^{2} \right) \\
\end{matrix}\) (6)

\section{\texorpdfstring{Mono-Symmetry Property, \(\mathbf{\beta}_{\mathbf{x}}\), for Singly-Symmetric I-Sections}{Mono-Symmetry Property, \textbackslash{}mathbf\{\textbackslash{}beta\}\_\{\textbackslash{}mathbf\{x\}\}, for Singly-Symmetric I-Sections}}\label{mono-symmetry-property-mathbfbetaux5fmathbfx-for-singly-symmetric-i-sections}

For the purpose of investigating the accuracy of the approximated \(\beta_{x}\) formula in Equation 5 and 6, the provided 281 doubly-symmetric W-sections available from the CISC Steel Design Handbook (aka ``the Handbook'') were used to generate 16312 singly-symmetric W-sections by separately reducing each flange by 10mm increments until each W-section merged into a T-section. The properties of these generated singly-symmetric WT-sections (i.e. flange completely disappears) should be highlighted in that they do not necessarily reflect true WT-sections that are used in structural engineering practice as their webs are generally too deep; however, they provide good estimates for all other monosymmetric sections.

Nonetheless, these generated W-sections provide a opportunity to discover the effect of the current \(\beta_{x}\) approximations. The analysis shows that as the mono-symmetry grows, so too does the percent error for the current \(\beta_{x}\) approximation. This effect can be seen in Figure 2 where the x-axis represents the state of the mono-symmetry property, \(\rho = \frac{I_{\text{yc}}}{I_{\text{yc}} + I_{\text{yt}}}\), and the y-axis represents the percent error of \(\beta_{x}\). The mono-symmetry parameter, \(\rho\), is defined such that for \(\rho = 0\) the section comprises of a inverted WT-section and for \(\rho = 1\) the section comprises of a WT-section.

\includegraphics{media/image2.png}

\hyperdef{}{ux5fRef505540339}{\label{ux5fRef505540339}}{}Figure Percent Error of \(\beta_{x}\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for all Made-Up Singly-Symmetric W-Sections

Figure 2 depicts significant errors when using the current form of the approximation (Eqn. 5 and 6) for \(\beta_{x}\) ({[}1{]}, {[}2{]}) of highly mono-symmetric W-sections. Looking closer at only the generated singly-symmetric W-sections which are geometrical representations of WT-sections, Figure 3 shows that the percent error is very similar.

\includegraphics{media/image3.png}

\hyperdef{}{ux5fRef505540452}{\label{ux5fRef505540452}}{}Figure Percent Error of \(\beta_{x}\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of all Classes of Sections

However, these WT-sections are not representative of those used in practice as these WT-sections are from the set of generated singly-symmetric W-sections and thus exist with deeper than normal webs. This makes it more likely that these generated WT-sections are Class 4 whereby local buckling would govern.

\includegraphics{media/image4.png}

\hyperdef{}{ux5fRef505540490}{\label{ux5fRef505540490}}{}Figure Number of Sections as a function of Section Class

Therefore, Figure 5 shows the percent error of only Class 1 and 2 generated WT-sections which reduces the number of sections considerably (i.e. many of the WT-sections generated end up as either Class 3 or 4) but the errors in \(\beta_{x}\) persist.

\includegraphics{media/image5.png}

\hyperdef{}{ux5fRef505540531}{\label{ux5fRef505540531}}{}Figure Percent Error of \(\beta_{x}\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Made-Up T-Sections of only Class 1 and 2 Sections

Since these WT-sections are not standard sizes, the next section of this technical note will investigate the effect of using the approximations defined above for \(\beta_{x}\) on standard WT-sections as provided from the Handbook.

\subsection{\texorpdfstring{Mono-Symmetry Property, \(\mathbf{\beta}_{\mathbf{x}}\), for standard WT-Sections}{Mono-Symmetry Property, \textbackslash{}mathbf\{\textbackslash{}beta\}\_\{\textbackslash{}mathbf\{x\}\}, for standard WT-Sections}}\label{mono-symmetry-property-mathbfbetaux5fmathbfx-for-standard-wt-sections}

For standard WT-sections from the Handbook, the mono-symmetry property is given in the tables provided. The mono-symmetry property in the Handbook is calculated to include the effect of fillets in either hot-rolled or cold-formed W- and WT-sections. As a simplification, this technical note neglects the effect of fillets on the mono-symmetry property (Eqn. 4) which is warranted as a comparison is conducted on the difference which can be seen in Figure 6. The effect of fillets on \(\beta_{x}\) is less than 1\% and therefore will be neglected within the scope of this technical note.\includegraphics{media/image6.png}

\hyperdef{}{ux5fRef505540626}{\label{ux5fRef505540626}}{}Figure Percent Difference of Including the Effect of Fillets in Hot-rolled or Cold-formed W- and WT-Sections on the Mono-symmetry property, \(\beta_{x}\)

For standard WT-sections, the approximations for \(\beta_{x}\) perform even worse with percent errors up to 14000\% as shown in Figure 7. Looking more closely at the distribution of section classes with respect to the \(\beta_{x}\) percent error, the section class does not to influence the accuracy of the \(\beta_{x}\) approximations as shown in Figure 7.

\includegraphics{media/image7.png}

\hyperdef{}{ux5fRef505540678}{\label{ux5fRef505540678}}{}Figure Percent Error of \(\beta_{x}\) based on Kitipornchai \& Trahair and CSA S16-14 Approximations for Standard WT-Sections for all Classes of Sections (left) and only Class 3 and 4 Sections (right)

\section{Discussion}\label{discussion}

From the analysis of the mono-symmetry of W-shaped beam cross-sections, it has been found that significant errors in the mono-symmetry parameter, \(\beta_{x}\), exist. The results will be explored on the basis of singly-symmetric W-sections and WT-sections separately. This technical note will explore potential mechanical/mathematical interpretation that may explain this phenomenon.

By plotting the \(\beta_{x}\) characteristic of a W1100x499 section as each flange decreases in width (thereby increasing the mono-symmetry), the shape of the \(\beta_{x}\) versus \(\rho\) curve becomes evident as seen in Figure 8. At the extreme mono-symmetric sections, there exists a slight decrease in the value of the mono-symmetry parameter, \(\beta_{x}\). This slight hook is present on all 281 doubly-symmetric sections when the flange widths are reduced. The hook occurs for both T-sections and inverted T-sections.

\includegraphics{media/image8.png}

\hyperdef{}{ux5fRef505540824}{\label{ux5fRef505540824}}{}Figure \(\beta_{x}\) as a function of \(\rho\) for a W1100x499 Section

Furthermore, on close inspection of the percent errors from the approximation by Kitipornchai and Trahair {[}2{]} and CSA S16-14 {[}1{]} it can be shown that the S16-14 approximation does not provide symmetrical results as compared to approximation by Kitipornchai and Trahair {[}2{]}. This means that the CSA S16-14 {[}1{]} approximation performs differently for inverted and non-inverted T-sections.

Mathematically, the percent error present for highly mono-symmetric sections by using the approximated formula exists since \(\beta_{x}\) is not linear over the entire interval of \(0 \leq \rho \leq 1.0\). This would raise the possibility of prescribing the approximated formula with limits based on \(\rho\) which is a potential solution; however, sections which do not fall within the said prescribed limits would need to be treated separately. Additionally, the technical note has analyzed the limits of \(0.1 \leq \rho \leq 0.9\) for the approximate \(\beta_{x}\) formula and Figure 9 shows the results of limiting the sections to these limits.

\includegraphics{media/image9.png}

\hyperdef{}{ux5fRef505541225}{\label{ux5fRef505541225}}{}Figure Percent Error of \(\beta_{x}\) Approximations limited by \(0.1 \leq \rho \leq 0.9\)

Additionally, from earlier analyses of W- and WT-sections in Figure 3 and Figure 5 there exists a significant trend whereby large \(\frac{I_{y}}{I_{x}}\) ratios tend towards higher errors in \(\beta_{x}\). Kitipornchai and Trahair {[}2{]} also limit the use of their developed approximation to sections with \(\frac{I_{y}}{I_{x}} \leq 0.5\). By limiting the ratio of \(\frac{I_{y}}{I_{x}}\) in conjunction with sections which do not fall within the limits of \(0.1 \leq \rho \leq 0.9\), it has been shown in Figure 9 that the percent errors for these types of sections can be reduced below 30\%.

The above mentioned solutions do not dismiss concerns about the suitability of the approximated forms of the \(\beta_{x}\) equations (Eqn. 5 and 6) but provide a temporary measure to limit the amount of error that is being introduced into the calculation of a elastic critical moment of mono-symmetrical beams. Further research is needed to provide a better approximated formula for \(\beta_{x}\) and quantifying the effect in more detail on the elastic critical moment capacity of singly-symmetric beams using the approximated \(\beta_{x}\) equations (Eqn. 5 and 6).

\subsection{Specific Discussion of WT-Sections}\label{specific-discussion-of-wt-sections}

The approximations for \(\beta_{x}\) in Equations 5 and 6 provide higher percent errors for WT-sections than for the ``made-up'' singly-symmetric sections previously discussed. The subplot on the left of Figure 10 shows the percent error as a function of the \(\frac{I_{y}}{I_{x}}\) ratio as all of these sections have a value of \(\rho = 1.0\). The remaining subplot on the right of Figure 10 shows the percent error again as a function of \(\frac{I_{y}}{I_{x}}\) ratio but limited at \(\frac{I_{y}}{I_{x}} < 0.5\).

\includegraphics{media/image10.png}

\hyperdef{}{ux5fRef505541569}{\label{ux5fRef505541569}}{}Figure Percent Error of \(\beta_{x}\) as a function of \(\frac{I_{y}}{I_{x}}\) Ratio for WT-Sections

The mathematical explanation for this result is that the \(\frac{I_{y}}{I_{x}}\) ratio is proportional to \(\beta_{x}\) by a power of two which dramatically increases the \(\beta_{x}\) for high values of the \(\frac{I_{y}}{I_{x}}\) ratio.

\section{Recommendation/Conclusion}\label{recommendationconclusion}

In conclusion, further in detail studies are required to determine a more appropriate approximation for \(\beta_{x}\). One potential avenue of research is to fit a splines to the true \(\beta_{x}\) curves with respect to the mono-symmetry, \(\rho\). However in the interim, it is recommended that the condition of \(\frac{I_{y}}{I_{x}} < 0.5\) be included in any use of the current approximations, particularly for WT-sections, as this was the limitation placed on the approximation by the original research by Kitipornchai and Trahair Kitipornchai and Trahair (1979). This technical note recommends that to limit the use of the current \(\beta_{x}\) approximation to \(0.1 < \rho < 0.9\) as significant errors are evident outside of these limits on singly-symmetric sections.

\section{References}\label{references}

{[}1{]} C. S16-14, ``Design of steel structures - Eight Edition; Incorporating Errata: October 2015; Update No. 1: December 2016,'' Canadian Standards Association, Standard, 2014.

{[}2{]} S. Kitipornchai and N. S. Trahair, ``Buckling properties of monosymmetric I-beams,'' 1979.

{[}3{]} T. V. Galambos, \emph{Structural members and frames}. Courier Dover Publications, 1968.

\end{document}
