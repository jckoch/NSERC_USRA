{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evaluation of CSA S16-14 Asymmetry Parameter for Standard WT-shapes\n",
    "\n",
    "Standard WT-shapes are taken from CISC *Excel Steel Section Tables* (version 9.2). $\\beta_x$ will be calculated using a exact expression and approximate formulas.\n",
    "\n",
    "### Table of Contents\n",
    "+ Set Up Python Environment\n",
    "+ Data Input\n",
    "+ Define Relevant Material Properties\n",
    "+ Section Property Calculatons\n",
    "+ Determine the Asymmetry Parameter, $\\beta_x$\n",
    "+ Determine % Difference of Calculating $\\beta_x$ with and without including Fillet Areas\n",
    "+ Determine % Error Between True and Approximate $\\beta_x$\n",
    "+ Write Data to a CSV file\n",
    "+ Section Class Statistics\n",
    "+ Percent Error Statistics\n",
    "+ Plotting the % Error for All Standard WT-Shapes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set Up Python Environment\n",
    "\n",
    "Import relevant python packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set up figure style as per the Canadian Journal of Civil Engineering specifications."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "def set_style():\n",
    "    # This sets reasonable defaults for font size for\n",
    "    # a figure that will go in a paper\n",
    "    sns.set_context(\"paper\")\n",
    "    \n",
    "    # Set the font to be serif, rather than sans\n",
    "    sns.set(font='serif')\n",
    "    \n",
    "    # Make the background white, and specify the\n",
    "    # specific font family\n",
    "    sns.set_style(\"white\", {\n",
    "        \"font.family\": \"serif\",\n",
    "        \"font.serif\": [\"Times\", \"Palatino\", \"serif\"]\n",
    "    })\n",
    "    \n",
    "# Single Column Plot (as per NRC CJCE specs)\n",
    "def set_size_onecol(fig):\n",
    "    fig.set_size_inches(9.3, 3.4)\n",
    "# Two Column Plot (as per NRC CJCE specs)\n",
    "def set_size_twocol(fig):\n",
    "    fig.set_size_inches(9.3, 7.2)\n",
    "\n",
    "# Matplotlib Default Parameters\n",
    "plt.rcParams['legend.fontsize'] = 'large'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Input\n",
    "\n",
    "Load section data for **standard** WT-shapes (included with the file \"CISC_SST_ver92_WT-cleaned.csv\"). Data loaded include width and thickness of compression and tension flanges, thickness of web, and overall height of beam.\n",
    "\n",
    "#### Notes on the Data\n",
    "+ It should be noted that the compression flange is assumed to be the top flange and the centriod of a cross-section is measured from the centriod of the top flange. \n",
    "+ All section properties are reported in millimetres (mm).\n",
    "+ Datum shall be top of the cross-section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = 'c:/jk/ualberta/Courses/project_DRA/analysis/CISC_StructuralSectionTables/CISC_SST_ver92_WT-cleaned.csv'\n",
    "(path,file) = os.path.split(filename)\n",
    "dfall = pd.read_csv(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define Relevant Material Properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fy = 350 # MPa\n",
    "sqrtFy = np.sqrt(Fy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Section Property Calculatons\n",
    "\n",
    "Assuming that the datum is the centroid of the top flange."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Determine the distance between flange centroids."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['d_prime'] = dfall['D'] - dfall['T']/2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Determine centroid of T-section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['yT_bar'] = dfall['T']/2 + dfall['Yo'] "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Determine major over minor moment of inertia ratio."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['Iy/Ix'] = dfall['Iy']/dfall['Ix']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Section Classification\n",
    "\n",
    "Section classification based on flange and web local buckling checks.\n",
    "Assuming a 350W Grade for steel, $F_y = 350MPa$.\n",
    "\n",
    "+ Set up S6-14 code requirements for flanges"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crit1 = 145\n",
    "crit2 = 170\n",
    "crit3 = 200"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "+ Determine section class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['Flange Criteria'] = ((dfall['B']/2)/dfall['T'])*np.sqrt(Fy)\n",
    "dfall['Flange Class'] = np.where(dfall['Flange Criteria'] < crit1, 1,\n",
    "                                 np.where(dfall['Flange Criteria'] < crit2, 2,\n",
    "                                          np.where(dfall['Flange Criteria'] < crit3, 3, 4)))\n",
    "dfall['Section Class'] = dfall['Flange Class']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determine the Asymmetry Parameter, $\\beta_x$\n",
    "\n",
    "Determine $\\beta_x$ from true expression and from approximate formula given in S16-14. Note that the true $\\beta_x$ is calculated in two steps where the flange contributions are determined first and then combined to achieve the true value.\n",
    "\n",
    "$\\beta_{x}$ will be calculated based on the true integral expression and various approximations.\n",
    "\n",
    "The integral (true) expression for $\\beta_{x}$ is calculated by:\n",
    "\n",
    "$\\beta_{x} = \\frac{1}{I_{x}} (\\int_{A}{x^{2}ydA} + \\int_{A}{y^{3}dA}) - 2y_{o}$\n",
    "\n",
    "Which can be further simplified to the following as per *Galambos (1968)*.\n",
    "\n",
    "$\\beta_{x} = \\frac{1}{I_x} ((h-\\bar{y})((h-\\bar{y})^3 \\frac{w}{4}) - \\bar{y}(\\frac{b_c^3 t_t}{12} + b_c t_c \\bar{y}^2 + \\bar{y}^3 \\frac{w}{4})) - 2y_o$\n",
    "\n",
    "Approximation for $\\beta_{x}$ is from *Kitipornchai & Trahair (1979) Buckling Properties of Monosymmetric I-Beams* and is calculated by:\n",
    "\n",
    "$\\beta_{x} = 0.9h(2\\rho - 1)(1 - (\\frac{I_y}{I_x})^{2})$, where $\\rho = \\frac{I_{yc}}{I_{yc} + I_{yt}}$\n",
    "\n",
    "Furthermore, $\\beta_{x}$ is calculated according to the CSA S16-14 standard which is computed by:\n",
    "\n",
    "$\\beta_{x} = 0.9(d-t)(\\frac{2I_{yc}}{I_{y}} - 1)(1 - (\\frac{I_{y}}{I_{x}})^{2})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['term1'] = (1/dfall['Ix'])\n",
    "dfall['term2'] = (dfall['W']/4)*((dfall['D'] - dfall['yT_bar'])**4 - (dfall['yT_bar'] - dfall['T']/2)**4)\n",
    "dfall['term3'] = dfall['B']*dfall['T']*(dfall['yT_bar'] - dfall['T']/2)*(dfall['B']**2/12 +\n",
    "                                                                         (dfall['yT_bar'] - dfall['T']/2)**2)\n",
    "dfall['term4'] = -2*dfall['Yo']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['BetX_calc'] = (dfall['term2'] - dfall['term3'])/dfall['Ix'] - dfall['term4']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['Iy_Flange1'] = ((dfall['B']**3)*dfall['T'])/12\n",
    "rho = 1.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['BetX_S16'] = (0.9*(dfall['D'] - dfall['T'])*((2*dfall['Iy_Flange1'])/dfall['Iy'] - 1) * \n",
    "                     (1 - (dfall['Iy']/dfall['Ix'])**2))\n",
    "dfall['BetX_K&T'] = (0.9*(dfall['D'] - dfall['T'])*(2*rho - 1)*(1 - (dfall['Iy']/dfall['Ix'])**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determine % Difference of Calculating $\\beta_x$ with and without including Fillet Areas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['% Difference with/without Including Fillets'] = (np.absolute(dfall['BetX_calc'] - dfall['BetX'])/dfall['BetX'])*100"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(dfall['% Difference with/without Including Fillets'])\n",
    "plt.xlabel('% Difference')\n",
    "plt.ylabel('Number of T-Sections')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determine % Error Between True and Approximate $\\beta_x$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall['Percent Error S16-14'] = ((dfall['BetX_S16'] - (dfall['BetX']))/np.absolute(dfall['BetX']))*100\n",
    "dfall['Percent Error K&T'] = ((dfall['BetX_K&T'] - (dfall['BetX']))/np.absolute(dfall['BetX']))*100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write Data to a CSV file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfall.to_csv('Handbook-Vs-Galambo_T-Section_BetX_Comparison.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Section Class Statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The number of Class 1, 2, and 3 sections are: %d' % len(dfall[dfall['Section Class'] != 4])) \n",
    "print('The number of Class 4 sections is: %d' % len(dfall[dfall['Section Class'] == 4]))\n",
    "print('Percentage of Class 4 Sections: %.2f' % \n",
    "      ((len(dfall[dfall['Section Class'] == 4])/len(dfall[dfall['Section Class'] != 4]))*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Percent Error Statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Maximum Percent Error using K&T Approximation: %.3f' % max(dfall['Percent Error K&T']))\n",
    "print('Maximum Percent Error using S16-14 Approximation: %.3f' % max(dfall['Percent Error S16-14']))\n",
    "print('Minimum Percent Error using K&T Approximation: %.3f' % min(dfall['Percent Error K&T']))\n",
    "print('Minimum Percent Error using K&T Approximation: %.3f' % min(dfall['Percent Error S16-14']))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('For sections with Iy/Ix < 0.5:')\n",
    "print('  + Minimum Percent Error using K&T Approximation: %.3f' % min(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error K&T']))\n",
    "print('  + Minimum Percent Error using S16-14 Approximation: %.3f' % \n",
    "      min(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error S16-14']))\n",
    "print('  + Maximum Percent Error using K&T Approximation: %.3f' % max(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error K&T']))\n",
    "print('  + Maximum Percent Error using S16-14 Approximation: %.3f' %\n",
    "      max(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error S16-14']))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the % Error for All Standard WT-Shapes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "+ Initialize and create Figure 4 in the technical note"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_style()\n",
    "fig4, ax4 = plt.subplots(nrows=1, ncols=2, sharey='row')\n",
    "mkr_dict_def = {1: 'Class 1', 2: 'Class 2', 3: 'Class 3', 4: 'Class 4'}\n",
    "mkr_dict = {1: '^', 2: 'o', 3: 'x', 4: '+'}\n",
    "color = {1: '#D3D3D3', 2: '#C0C0C0', 3: '#808080', 4: '#303030'}\n",
    "for kind in mkr_dict:\n",
    "    dd1 = dfall[dfall['Section Class'] == kind]\n",
    "    dd2 = dd1[dd1['Iy/Ix'] < 1.0]\n",
    "    ax4[0].scatter(dd2['Iy/Ix'], dd2['Percent Error K&T'], color=color[kind], marker=mkr_dict[kind], label=mkr_dict_def[kind])\n",
    "    ax4[0].tick_params(axis='both', which='major', labelsize=12)\n",
    "    ax4[1].scatter(dd2['Iy/Ix'], dd2['Percent Error S16-14'], color=color[kind], marker=mkr_dict[kind], label=mkr_dict_def[kind])\n",
    "    ax4[1].tick_params(axis='both', which='major', labelsize=12)\n",
    "\n",
    "# Set the y-axis label only for the left subplot\n",
    "ax4.flat[0].set_ylabel('Percent Error', fontsize='12')\n",
    "ax4.flat[0].set_xlabel(r'$\\frac{I_y}{I_x}$', fontsize='18')\n",
    "ax4.flat[1].set_xlabel(r'$\\frac{I_y}{I_x}$', fontsize='18')\n",
    "\n",
    "# Set x and y limits\n",
    "ax4.flat[0].set_ylim((-85,25))\n",
    "ax4.flat[0].set_xticks((0,0.2,0.4,0.6,0.8,1.0))\n",
    "ax4.flat[1].set_xticks((0,0.2,0.4,0.6,0.8,1.0))\n",
    "\n",
    "# Set the labels for each column\n",
    "ax4.flat[0].text(0.5,-0.45, \"(a)\", size=12, ha=\"center\", transform=ax4.flat[0].transAxes)\n",
    "ax4.flat[1].text(0.5,-0.45, \"(b)\", size=12, ha=\"center\", transform=ax4.flat[1].transAxes)\n",
    "\n",
    "# Create Legend\n",
    "plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), fontsize='12')\n",
    "\n",
    "# Set size as per CJCE requirements\n",
    "set_size_onecol(fig4)\n",
    "\n",
    "# Set up tight plot layout\n",
    "plt.tight_layout()\n",
    "\n",
    "# Save Figure 4 as PNG and PDF and SVG\n",
    "plt.savefig('./paper_figs/Figure4.png', bbox_inches=\"tight\")\n",
    "plt.savefig('./paper_figs/Figure4.pdf', bbox_inches=\"tight\")\n",
    "plt.savefig('./paper_figs/Figure4.svg', bbox_inches=\"tight\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
