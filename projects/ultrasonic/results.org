#+OPTIONS: H:2 toc:nil todo:nil tags:nil *:t ^:nil 
#+Title: Ultrasonic Residual Stress Measurement - Repeatability Testing Results
#+Author: James C Koch
#+Email: jckoch@ualberta.ca
#+LATEX_HEADER: \usepackage[backend=bibtex,sorting=none, style=numeric, citestyle=numeric]{biblatex}
#+LATEX_HEADER: \usepackage[margin=0.7in]{geometry}
#+LATEX_HEADER: \graphicspath{ {./figs/} }
#+BEAMER_THEME: Boadilla
#+BEAMER_FRAME_LEVEL: 2

* Introduction
Testing of the ultrasonic non-destructive residual stress measurement for use in the Steel Centre LTB project.
The system currently has a problem with repeatability of measurements specifically when the transducer wedges are removed and couplant is reapplied.
* Experimental Set Up & Observations
The current experimental setup consists of two transducers, a DPR (ultrasonic pulser), a oscilloscope, and a laptop computer.
The two transducers are set in a plastic wedge to guide the ultrasonic waves into the steel test specimen.
Some initial observations include that time-of-flight (TOF) measurements tend to decrease over time which is thought to be due to the squishing of the couplant layer. 
Additionally, applying pressure to the wedges may decrease the TOF measurements.
This is thought to be because the couplant layer becomes thinner due to the applied pressure.
Two types of couplant were used and their effectiveness in terms of measurement repeatability (i.e. re-application of couplant) was compared.
* Results
In all cases, the mean and standard deviation time of flight in nanoseconds from origin transducer and receiving transducer was measured.
Additionally, the mean and standard deviation of each test is reported.
Between each test, the transducer wedges were cleaned of all couplant and new couplant was reapplied.
** Test 1
This test occurred on [2018-05-10 Thu] and included 5 repeatability tests with couplant of Type B while *no* pressure was applied on the transducer wedges.
Table ref:TestDay01 shows the results for the 5 repeatability tests *without* applying pressure on the transducer wedges.
#+NAME: TestDay01
#+CAPTION: Mean and Std. Dev. TOF using Couplant Type B while applying no pressure \label{TestDay01}
|     Test | Mean TOF (ns) | Std TOF (ns) | N (Iterations) |
|----------+---------------+--------------+----------------|
| 0510-007 |       37565.1 |      1.53630 |             60 |
| 0510-008 |       37521.2 |      0.79440 |             60 |
| 0510-009 |       37510.2 |      1.21020 |             60 |
| 0510-010 |       37531.6 |      0.71639 |             60 |
| 0510-011 |       37536.5 |      0.56897 |             60 |
|----------+---------------+--------------+----------------|
|  stdev = |     20.637757 |   0.39820141 |                |
|   mean = |      37532.92 |     0.965252 |                |
#+TBLFM: @>$2=vmean(@I..@II)::@>$3=vmean(@I..@II)
#+TBLFM: @7$2=vsdev(@I..@II)::@7$3=vsdev(@I..@II)
** Test 1 (cont')
The 5 repeatability tests were determined to have a standard deviation of 20 nanoseconds whereas the expected and desired is lower at 1 nanoseconds.
Using a housing for the transducers which would prevent the transducers from drifting apart (i.e. due to the viscous but not solid couplant layer) the ultimate goal of 1ns repeatability is more feasible.
There is a trend for the wedges without the housing to slowly drift apart and distance the ultrasonic wave travels is arguably a greater variable with respect to time-of-f
One further avenue to explore may be to investigate the effect of "setting time" for the couplant layer.
** Test 2
This test occurred on [2018-05-11 Fri] and included 5 repeatability tests with couplant Type B while pressure was applied by hand on the transducer wedges.
Table ref:TestDay02 shows the results for the 6 repeatability tests conducted while applying pressure by hand on the transducer wedges.
#+NAME: TestDay02
#+CAPTION: Mean and Std. Dev. TOF using Couplant Type B while applying pressure \label{TestDay02}
|     Test | Mean TOF (ns) | Std TOF (ns) | N (Iterations) |
|----------+---------------+--------------+----------------|
| 0511-007 |       37456.1 |       1.4944 |             30 |
| 0511-009 |       37483.6 |     0.514716 |             33 |
| 0511-010 |       37534.0 |      1.88753 |             32 |
| 0511-011 |       37536.4 |      1.31544 |             33 |
| 0511-012 |       37553.0 |      1.11126 |             32 |
| 0511-013 |       37526.8 |      1.40207 |             32 |
|----------+---------------+--------------+----------------|
|  stdev = |     37.033413 |   0.45716653 |                |
|   mean = |     37514.983 |    1.2875693 |                |
#+TBLFM: @>$2=vmean(@I..@II)::@>$3=vmean(@I..@II)
#+TBLFM: @8$2=vsdev(@I..@II)::@8$3=vsdev(@I..@II)
** Test 2 (cont')
It was found that applying pressure on the transducer wedges by hand is difficult task.
This stems primarily from the difficulty in maintaining a constant applied pressure on the transducer wedges.
The standard deviation over all 6 tests conducted while applying pressure is larger for the tests where pressure was applied most likely due to the above difficulty.
The mean time of flight over all 5 repeatability tests was lower for the tests with applied pressure than without applied pressure but the difference is within the standard deviation of the mean over the 5 tests and is questionable as to it's significance.
** Test 3
This test occurred on [2018-05-15 Tue] and included 8 repeatability tests with couplant Type D while *no* pressure was applied on the transducer wedges.
Table ref:TestDay3-01 shows the results for these 8 repeatability tests *without* applying pressure on the transducer wedges.
#+NAME: TestDay3-01
#+CAPTION: Mean and Std. Dev. TOF using Couplant Type D while applying no pressure \label{TestDay3-01}
|     Test | Mean TOF (ns) | Std TOF (ns) | N (Iterations) |
|----------+---------------+--------------+----------------|
| 0515-001 |       37567.8 |      5.69581 |            126 |
| 0515-002 |       37538.1 |      1.10920 |            125 |
| 0515-003 |       37547.5 |      1.40036 |            127 |
| 0515-004 |       37548.3 |      2.75150 |            147 |
| 0515-005 |       37533.0 |      1.79901 |            131 |
| 0515-006 |       37569.6 |      1.41877 |            125 |
| 0515-007 |       37556.4 |      1.98815 |            126 |
| 0515-008 |       37554.2 |      3.88549 |            130 |
|----------+---------------+--------------+----------------|
|  stdev = |     12.937204 |    1.5703372 |                |
|   mean = |     37551.863 |    2.5060363 |                |
#+TBLFM: @>$2=vmean(@I..@II)::@>$3=vmean(@I..@II)
#+TBLFM: @10$2=vsdev(@I..@II)::@10$3=vsdev(@I..@II)
The standard deviation over all 8 repeatability tests was 13ns which is lower than using couplant Type B by one third of the standard deviation in [[Test 1]].
It seems that couplant Type D requires ~10s to set, to get stable readings sooner; however, more investigation is needed to definitively say this.
** Test 4
This test occurred on [2018-05-15 Tue] and included 8 repeatability tests with couplant Type D while pressure was applied by hand on the transducer wedges.
Table ref:TestDay3-02 shows the results for the 8 repeatability tests conducted while applying pressure by hand on the transducer wedges.
#+NAME: TestDay3-02
#+CAPTION: Mean and Std. Dev. TOF using Couplant Type D while applying pressure \label{TestDay3-02}
|     Test | Mean TOF (ns) | Std TOF (ns) | N (Iterations) |
|----------+---------------+--------------+----------------|
| 0515-009 |       37565.0 |     1.495380 |             30 |
| 0515-010 |       37604.2 |     1.553480 |             32 |
| 0515-011 |       37612.8 |     1.110860 |             28 |
| 0515-012 |       37601.4 |     0.929238 |             26 |
| 0515-013 |       37599.1 |     1.350730 |             28 |
| 0515-014 |       37660.8 |     1.600180 |             31 |
| 0515-015 |       37627.4 |     1.061570 |             27 |
| 0515-016 |       37630.0 |     1.363770 |             30 |
|----------+---------------+--------------+----------------|
|  stdev = |     28.002777 |   0.24748142 |                |
|   mean = |     37612.588 |     1.308151 |                |
#+TBLFM: @>$2=vmean(@I..@II)::@>$3=vmean(@I..@II)
#+TBLFM: @10$2=vsdev(@I..@II)::@10$3=vsdev(@I..@II)
Again, while applying pressure by hand the standard deviation over the 8 tests increased with respect to [[Test 3]] which indicates that uncertainty is being introduced due to not applying constant pressure.
Much smaller standard deviations in each test and over the 8 tests were recorded.
This would indicate that applying pressure is helpful for precision but not necessarily for accuracy.
* Conclusion
The preliminary results from these tests indicate that while applying pressure can be helpful; it is only helpful when *constant* pressure is applied which can be difficult to do by hand.
Couplant Type B had lower standard deviations in time-of-flight measurements than couplant Type D.
However, couplant Type D had lower standard deviations in time-of-flight measurements when pressure was applied on the transducer wedges which as previously mentioned is an indication that applying pressure 
This should be tested once the housing for the transducer wedges is built and ready for use.
Furthermore, couplant Type D provided better repeatability with 12ns compared to 20ns in [[Test 1]] and [[Test 3]] (both tests without applying pressure).
In conclusion due to the tendency for the wedges to drift apart and unable to provide constant applied pressure, it is recommended that more tests are conducted with the housing in place to accurately determine these effects.
